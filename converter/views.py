from typing import Any, Dict
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.generic import FormView
from .forms import ConverterForm
from .mozzi import char2mozzi


class ConverterFormView(FormView):
  
  form_class = ConverterForm
  template_name = "converter/form.html"
  success_url = '/'
  
  def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
    context: dict = super().get_context_data(**kwargs)
    context['title'] = "Audio Sample Converter for Arduino/Mozzi development"
    return context
  
  def post(self, request: HttpRequest, *args, **kwargs):
      form_class = self.get_form_class()
      form = self.get_form(form_class)
      if form.is_valid():
        # TODO send to a template that provides instructions on including the .h file, displays the .h file in a textarea,
        # and provides a data URL with attachment content-disposition
        # (https://lists.w3.org/Archives/Public/uri/2010Feb/0069.html)
        outfile = char2mozzi(form.cleaned_data['infile'], form.cleaned_data['tablename'], form.cleaned_data['samplerate'])
        resp = HttpResponse(outfile)
        resp['content-type'] = 'text/plain'
        resp['content-length'] = len(resp.content)
        resp['content-disposition'] = f"attachment; filename=\"{form.cleaned_data['outfile']}\""
        return resp
      
      else:
        print(form.cleaned_data)
        return self.form_invalid(form)
        
  def form_valid(self, form):
    return super().form_valid(form)