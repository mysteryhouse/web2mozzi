from django.forms import ClearableFileInput, Form, fields

class ConverterForm(Form):
  infile = fields.FileField(label="File to Convert", required=True, help_text="(Should be a signed, 8-bit PCM audio .wav file)")
  outfile = fields.CharField(required=True)
  tablename = fields.CharField(label="Table Name", required=True)
  samplerate = fields.IntegerField(label="Sample Rate", initial=16384, required=True, help_text="(The sample rate at which the original file was recorded)")
  
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    for f in ['infile', 'outfile', 'tablename', 'samplerate']:
      self.fields[f].widget.attrs.update({"class": "form-control required"})
    self.fields['infile'].widget.attrs.update({"onChange": "suggestValues();", "class": "form-control"})
    